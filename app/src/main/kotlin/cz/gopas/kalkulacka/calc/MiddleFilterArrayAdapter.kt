package cz.gopas.kalkulacka.calc

import android.content.Context
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.Filter

class MiddleFilterArrayAdapter(context: Context, val data: List<CharSequence>) :
    ArrayAdapter<CharSequence>(context, android.R.layout.simple_dropdown_item_1line, data.toMutableList()) {

    private val filter = MiddleFilter()

    override fun getFilter() = filter

    inner class MiddleFilter : Filter() {
        override fun performFiltering(constraint: CharSequence?) = FilterResults().apply {
            val filtered: List<CharSequence> = data.filter { it.contains(constraint ?: "", true) }
            Log.d(TAG, "Filtering $constraint: $filtered")
            values = filtered
            count = filtered.size
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            Log.d(TAG, "Got results: ${results?.values as List<CharSequence>}")
            clear()
            addAll(results.values as List<CharSequence>)
        }
    }

    private companion object {
        private val TAG = MiddleFilterArrayAdapter::class.simpleName
    }
}