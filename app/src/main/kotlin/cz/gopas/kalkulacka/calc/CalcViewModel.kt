package cz.gopas.kalkulacka.calc

import android.app.Application
import androidx.annotation.IdRes
import androidx.core.content.edit
import androidx.lifecycle.AndroidViewModel
import androidx.preference.PreferenceManager
import cz.gopas.kalkulacka.R
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlin.concurrent.thread

class CalcViewModel(app: Application) : AndroidViewModel(app) {
    private val _result = MutableStateFlow<Float?>(null)
    val result: StateFlow<Float?> = _result

    private val prefs = PreferenceManager.getDefaultSharedPreferences(app)

    val ans = MutableStateFlow<Float?>(null)

    fun calc(a: Float, b: Float, @IdRes op: Int) {
        thread {
            Thread.sleep(1000)
            _result.value = when (op) {
                R.id.add -> a + b
                R.id.sub -> a - b
                R.id.div -> a / b
                R.id.mul -> a * b
                else -> Float.NaN
            }.also { res ->
                prefs.edit {
                    putFloat(ANS_KEY, res)
                }
            }
        }
    }

    fun ans() {
        ans.value = prefs.getFloat(ANS_KEY, Float.NaN)
    }

    private companion object {
        private const val ANS_KEY = "ans"
    }
}