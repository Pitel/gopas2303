package cz.gopas.kalkulacka.calc

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ArrayAdapter
import androidx.core.view.MenuProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.commit
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import cz.gopas.kalkulacka.R
import cz.gopas.kalkulacka.about.AboutFragment
import cz.gopas.kalkulacka.databinding.FragmentCalcBinding
import cz.gopas.kalkulacka.history.HistoryFragment
import cz.gopas.kalkulacka.history.HistoryViewModel
import cz.gopas.kalkulacka.history.db.HistoryEntity
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class CalcFragment : Fragment(), MenuProvider {
    private var binding: FragmentCalcBinding? = null
    private val viewModel by viewModels<CalcViewModel>()
    private val historyViewModel by activityViewModels<HistoryViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentCalcBinding.inflate(inflater, container, false)
        .also { binding = it }
        .root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding?.run {
            calc.setOnClickListener {
                calc()
            }

            share.setOnClickListener {
                share()
            }

            ans.setOnClickListener {
                viewModel.ans()
            }

            history.setOnClickListener {
                parentFragmentManager.commit {
                    replace(R.id.container, HistoryFragment())
                    addToBackStack(null)
                }
            }

            viewModel.result.filterNotNull().onEach {
                res.text = "$it"
                historyViewModel.db.insert(HistoryEntity(it))
            }.launchIn(viewLifecycleOwner.lifecycleScope)

            viewModel.ans.filterNotNull().onEach {
                aText.setText("$it")
                viewModel.ans.value = null
            }.launchIn(viewLifecycleOwner.lifecycleScope)

            autocomplete.setAdapter(
                MiddleFilterArrayAdapter(
                    requireContext(),
                    listOf("Jablka", "Hrušky")
                )
            )
        }
        requireActivity().addMenuProvider(this, viewLifecycleOwner)
    }

    override fun onStart() {
        super.onStart()
        historyViewModel.selected.filterNotNull().onEach {
            Log.d(TAG, "Got $it from history")
            binding?.bText?.setText("$it")
            historyViewModel.selected.value = null
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }

    override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
        menuInflater.inflate(R.menu.menu, menu)
    }

    override fun onMenuItemSelected(menuItem: MenuItem) = when (menuItem.itemId) {
        R.id.about -> {
            parentFragmentManager.commit {
                replace(R.id.container, AboutFragment())
                addToBackStack(null)
            }
            true
        }
        else -> false
    }

    private fun calc() {
        Log.d(TAG, "Calc")

        val a = binding!!.aText.text.toString().toFloatOrNull() ?: Float.NaN
        val b = binding!!.bText.text.toString().toFloatOrNull() ?: Float.NaN
        viewModel.calc(a, b, binding!!.ops.checkedRadioButtonId)
//        binding!!.res.text = when (binding!!.ops.checkedRadioButtonId) {
//            R.id.add -> a + b
//            R.id.sub -> a - b
//            R.id.mul -> a * b
//            R.id.div -> {
//                if (b == 0f) {
//                    ZeroDialog().show(childFragmentManager, null)
//                }
//                a / b
//            }
//            else -> Float.NaN
//        }.toString()
    }

    private fun share() {
        val intent = Intent(Intent.ACTION_SEND)
            .setType("text/plain")
            .putExtra(Intent.EXTRA_TEXT, getString(R.string.share_text, binding!!.res.text))
        startActivity(intent)
    }

    private companion object {
        private val TAG = CalcFragment::class.simpleName
    }
}