package cz.gopas.kalkulacka.about

import androidx.fragment.app.Fragment
import cz.gopas.kalkulacka.MainActivity
import cz.gopas.kalkulacka.R

class AboutFragment : Fragment(R.layout.fragment_about) {
    init {
        MainActivity.LEAK += this
    }
}