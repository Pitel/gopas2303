package cz.gopas.kalkulacka.history

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.room.Room
import cz.gopas.kalkulacka.history.db.HistoryDatabase
import kotlinx.coroutines.flow.MutableStateFlow

class HistoryViewModel(app: Application) : AndroidViewModel(app) {
    val selected = MutableStateFlow<Float?>(null)

    val db = Room.databaseBuilder(app, HistoryDatabase::class.java, "history")
//        .allowMainThreadQueries()
        .build()
        .historyDao()
}