package cz.gopas.kalkulacka.history

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import cz.gopas.kalkulacka.R
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class HistoryFragment : Fragment(R.layout.fragment_history) {
    private val historyViewModel by activityViewModels<HistoryViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val recycler = view as RecyclerView
        recycler.setHasFixedSize(true)
        val adapter = HistoryAdapter {
            historyViewModel.selected.value = it
            parentFragmentManager.popBackStack()
        }
        recycler.adapter = adapter
        historyViewModel.db.getAll().onEach {
            adapter.submitList(it)
        }.launchIn(viewLifecycleOwner.lifecycleScope)
//        viewLifecycleOwner.lifecycleScope.launch {
//            val list = mutableListOf<HistoryEntity>()
//            var i = 0
//            while (isActive) {
//                delay(500)
//                list += HistoryEntity(i.toFloat(), i.toLong())
//                i++
//                adapter.submitList(list.toList())
//            }
//        }
//        adapter.submitList(List(100_000) { HistoryEntity(it.toFloat(), it.toLong()) })
    }
}