package cz.gopas.kalkulacka

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.os.LocaleListCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import java.util.*

class MainActivity : AppCompatActivity() {
//    private val aText by lazy { findViewById<TextInputEditText>(R.id.a_text) }
//    private val bText by lazy { findViewById<TextInputEditText>(R.id.b_text) }
//    private val ops by lazy { findViewById<RadioGroup>(R.id.ops) }
//    private val res by lazy { findViewById<MaterialTextView>(R.id.res) }

    init {
        FragmentManager.enableDebugLogging(BuildConfig.DEBUG)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppCompatDelegate.setApplicationLocales(LocaleListCompat.create(Locale.GERMAN))
        setContentView(R.layout.activity_main)
//        if (savedInstanceState == null) {
//            supportFragmentManager.commit {
//                replace(android.R.id.content, CalcFragment())
//            }
//        }
        Toast.makeText(this, R.string.app_name, Toast.LENGTH_SHORT).show()
        Log.d(TAG, "Create")
        Log.d(TAG, "Intent ${intent.dataString}")

        val request = Request.Builder()
            .url(getString(R.string.api))
            .build()
        lifecycleScope.launch(Dispatchers.IO) {
            HTTP.newCall(request).execute()
        }
    }

//    override fun onSaveInstanceState(outState: Bundle) {
//        super.onSaveInstanceState(outState)
//        outState.putCharSequence(RES_KEY, res.text)
//    }
//
//    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
//        super.onRestoreInstanceState(savedInstanceState)
//        res.text = savedInstanceState.getCharSequence(RES_KEY)
//    }

    override fun onStart() {
        super.onStart()
        Toast.makeText(this, "Start", Toast.LENGTH_SHORT).show()
        Log.d(TAG, "Start")
    }

    override fun onPause() {
        super.onPause()
        Toast.makeText(this, "Pause", Toast.LENGTH_SHORT).show()
        Log.d(TAG, "Pause")
    }

    override fun onStop() {
        super.onStop()
        Toast.makeText(this, "Stop", Toast.LENGTH_SHORT).show()
        Log.d(TAG, "Stop")
    }

    companion object {
        private val TAG = MainActivity::class.simpleName
        private const val RES_KEY = "res"
        val LEAK = mutableListOf<Fragment>()
        val HTTP = OkHttpClient.Builder()
            .addNetworkInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .build()
    }
}